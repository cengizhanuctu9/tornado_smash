using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tornado : MonoBehaviour
{
    public Transform tornadotransform;

    public float pullforce;
    public float timer;

    private void Update()
    {

        transform.Rotate(0, 6, 0);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("cube"))
        {
            StartCoroutine(pullobje(other,true));
           
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("cube"))
        {
            StartCoroutine(pullobje(other,false));
        }
    }

    IEnumerator pullobje(Collider obj,bool shuldpull)
    {
        if (shuldpull&&obj!=null)
        {

            Vector3 dir = obj.transform.position - tornadotransform.position;
            obj.GetComponent<Rigidbody>().AddForce(dir.normalized * pullforce * Time.deltaTime);
            

            yield return timer;
            StartCoroutine(pullobje(obj, shuldpull));
        }

    }

}
