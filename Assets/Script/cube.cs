
using UnityEngine;

public class cube : MonoBehaviour
{
    float scalemin = 1;
    float speed = 11;
    
   
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("tornado"))
        {

            scalemin -= Time.deltaTime * speed;
            if (scalemin >= 0.2)
            {

                transform.localScale = new Vector3(scalemin, scalemin, scalemin);


            }

            else if (scalemin <= 0)
            {
               
                Destroy(gameObject);
            }


        }
        if (other.CompareTag("wall"))
        {
          
            Destroy(gameObject);
        }
    }

}
