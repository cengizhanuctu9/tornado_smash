using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wall : MonoBehaviour
{
    public float force;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("cube"))
        {
           Rigidbody rb= other.GetComponent<Rigidbody>();
            rb.AddForce(0, force, 0);
        }
    }
}
